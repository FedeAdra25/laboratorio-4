class CreateVictimas < ActiveRecord::Migration[6.0]
  def change
    create_table :victimas do |t|
      t.string :nombre
      t.integer :monstruo_id

      t.timestamps
    end
  end
end
