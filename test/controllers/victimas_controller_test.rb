require 'test_helper'

class VictimasControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get victimas_index_url
    assert_response :success
  end

  test "should get show" do
    get victimas_show_url
    assert_response :success
  end

end
