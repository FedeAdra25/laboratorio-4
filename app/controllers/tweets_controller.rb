# encoding = utf-8
class TweetsController < ApplicationController
  
  def new
  	@tweet = Tweet.new
  end

  def default    
  end
  
  def index
  end

  def show
  end

  # -----------------
  # CREATE
  # El método create recibe params = { tweet: { monstruo_id: valor, estado: valor } }
  def create

    # tweet.monstruo_id=params[:tweet][:monstruo_id]
    # tweet.estado=params[:tweet][:estado]
    # tweet.save
    # redirect_to root_path
    @tweet=Tweet.new(params.require(:tweet).permit(:monstruo_id,:estado))
    byebug
    if @tweet.save
      redirect_to root_path, notice: "Tweet creado exitosamente"
    else
      flash[:error]="ERROR AL CARGAR"
      render :new
    end
  end
end
