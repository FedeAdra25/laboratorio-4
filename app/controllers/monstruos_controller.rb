class MonstruosController < ApplicationController
  
  def index
  end

  def show
    @monstruo=Monstruo.find(params[:id])
     if @monstruo.tweets.empty?
       flash[:error]="ERROR AL CARGAR"
       redirect_to monstruos_path,
       status=>
          :found,
          :notice => "¡No se han encontrado twits para ese monstruo!"
          #,action=>"controllerName"
     end


  end

  def edit    
  end
  
  def destroy
    m=Monstruo.find(params[:id])
    m.destroy
    redirect_to monstruo_path
  end
end
