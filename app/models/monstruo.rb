class Monstruo < ApplicationRecord
  has_many :victimas, dependent: :destroy
  has_many :tweets, dependent: :destroy

  default_scope -> { order :nombre }
end
